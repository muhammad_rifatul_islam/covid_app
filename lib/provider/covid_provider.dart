
       import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

      import 'package:http/http.dart' as Http;

import '../model/covid_model.dart';

class CovidInfoProvider with ChangeNotifier{

     CovidModel? _covidModel;
     String _country='bd';

     CovidModel? get covidModel => _covidModel;


     String get country => _country;

  set country(String value) {
    _country = value;
  }

  Future<void>  getInfo() async{

          String url='https://disease.sh/v3/covid-19/countries/$country';
    try{
      final response=   await Http.get(Uri.parse(url));
         final map=json.decode(response.body);
         if(response.statusCode ==200){
           _covidModel=CovidModel.fromJson(map);
           notifyListeners();
         }else{
           Fluttertoast.showToast(msg: '${response.statusCode}',
               gravity: ToastGravity.CENTER,
               backgroundColor: Colors.red,
               textColor: Colors.white);
         }
         }catch(error){
      throw error;
    }
    }
       }

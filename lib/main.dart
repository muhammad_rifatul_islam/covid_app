import 'package:covid_19/pages/covidInfoPage.dart';
import 'package:covid_19/pages/splash_page.dart';
import 'package:covid_19/provider/covid_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) =>CovidInfoProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Covid 19 App',
        theme: ThemeData(

          primarySwatch: Colors.blue,
        ),
        home: SplashScreen(),
        routes: {

          SplashScreen.routeName:(context)=>SplashScreen(),
          CovidInfoPage.routeName:(context)=>CovidInfoPage(),
        },
      ),
    );
  }
}

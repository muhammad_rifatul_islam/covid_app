
import 'package:fl_chart/fl_chart.dart';
import 'package:fl_country_code_picker/fl_country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/covid_provider.dart';
import '../utils/helper_function.dart';
import '../widget/indicator.dart';

class CovidInfoPage extends StatefulWidget {
  static const String routeName='/covidInfo_page';
  const CovidInfoPage({Key? key}) : super(key: key);

  @override
  State<CovidInfoPage> createState() => _CovidInfoPageState();
}

class _CovidInfoPageState extends State<CovidInfoPage> {
  int touchedIndex = -1;
  bool call = true;
  late CovidInfoProvider _covidInfoProvider;
  final countryPicker = const FlCountryCodePicker();

  @override
  void didChangeDependencies() {
    if (call) {
      _covidInfoProvider = Provider.of<CovidInfoProvider>(context);
      _covidInfoProvider.getInfo();
      call = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    /*  print((_covidInfoProvider.covidModel!.testsPerOneMillion!/10000));
        print(_covidInfoProvider.covidModel!.recoveredPerOneMillion!/10000);
        print(_covidInfoProvider.covidModel!.deathsPerOneMillion!/10000);
        print(_covidInfoProvider.covidModel!.casesPerOneMillion!/10000);*/
    return Scaffold(

        backgroundColor: const Color(0xb77caad8),
        body: _covidInfoProvider.covidModel != null ? Padding(
          padding: const EdgeInsets.all(8.0),
          child: SafeArea(
            child: ListView(
              children: [
                const SizedBox(height: 30,),
                Column(
                  children: [


                    Row(
                      mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('${_covidInfoProvider.covidModel!.country}',
                          style: const TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Colors.white
                          ),),
                        GestureDetector(
                          onTap: () async {
                            final code = await countryPicker.showPicker(context: context);
                            if (code != null)  print(code);
                            _covidInfoProvider.country=code!.code;
                            _covidInfoProvider.getInfo();
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 4.0),
                            margin: const EdgeInsets.symmetric(horizontal: 8.0),
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(5.0))),
                            child:  Row(
                              children: [
                                Text(_covidInfoProvider.country.toUpperCase(),
                                    style: const TextStyle(color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25)),
                                Icon(Icons.arrow_drop_down,color: Colors.black,size: 30,)
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Text('Last Update:${getFormatedDate(
                        _covidInfoProvider.covidModel!.updated!,
                        'E, d MMM yyyy HH:mm a')}',
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),),

                    const SizedBox(height: 10,),
                    Image.network(
                      _covidInfoProvider.covidModel!.countryInfo!.flag!,
                      height: 100,),

                    AspectRatio(
                      aspectRatio: 1.5,
                      child: Padding(

                        padding: const EdgeInsets.all(16.0),
                        child: Card(
                          color: Colors.white,
                          child: Row(
                            children: <Widget>[
                              const SizedBox(
                                height: 18,
                              ),
                              Expanded(
                                child: AspectRatio(
                                  aspectRatio: 1.3,
                                  child: PieChart(
                                    PieChartData(
                                        pieTouchData: PieTouchData(touchCallback:
                                            (FlTouchEvent event, pieTouchResponse) {
                                          setState(() {
                                            if (!event
                                                .isInterestedForInteractions ||
                                                pieTouchResponse == null ||
                                                pieTouchResponse.touchedSection ==
                                                    null) {
                                              touchedIndex = -1;
                                              return;
                                            }
                                            touchedIndex = pieTouchResponse
                                                .touchedSection!
                                                .touchedSectionIndex;
                                          });
                                        }),
                                        borderData: FlBorderData(
                                          show: false,
                                        ),
                                        sectionsSpace: 0,
                                        centerSpaceRadius: 45,
                                        sections: showingSections()),
                                  ),
                                ),
                              ),
                              Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const <Widget>[
                                  Indicator(
                                    color: Color(0xff0293ee),
                                    text: 'Cases/M',
                                    isSquare: true,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Indicator(
                                    color: Colors.red,
                                    text: 'Deaths/M',
                                    isSquare: true,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Indicator(
                                    color: Color(0xff845bef),
                                    text: 'Tests/M',
                                    isSquare: true,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Indicator(
                                    color: Colors.green,
                                    text: 'Recovered/M',
                                    isSquare: true,
                                  ),
                                  SizedBox(
                                    height: 18,
                                  ),
                                ],
                              ),
                              const SizedBox(
                                width: 28,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),),
                          elevation: 7,
                          child: Container(

                            height: 100,
                            width: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.amber
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  const Text('Affected', style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white
                                  ),),
                                  Text('${_covidInfoProvider.covidModel!.cases}',
                                    style: const TextStyle(
                                        fontSize: 25,
                                        color: Colors.white
                                    ),)
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 20,),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),),
                          elevation: 7,
                          child: Container(
                            height: 100,
                            width: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.red
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  const Text('Deaths', style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white
                                  ),),
                                  Text('${_covidInfoProvider.covidModel!.deaths}',
                                    style: TextStyle(
                                        fontSize: 25,

                                        color: Colors.white
                                    ),)
                                ],
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),),
                          elevation: 7,
                          child: Container(

                            height: 100,
                            width: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.green
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  const Expanded(
                                    child: Text('Recovered', style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white
                                    ),),
                                  ),
                                  Expanded(
                                    child: Text('${_covidInfoProvider.covidModel!
                                        .recovered}', style: const TextStyle(
                                        fontSize: 25,
                                        color: Colors.white
                                    ),),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 20,),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),),
                          elevation: 7,
                          child: Container(
                            height: 100,
                            width: 150,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.blue
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  const Text('Active', style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white
                                  ),),
                                  Text('${_covidInfoProvider.covidModel!.active}',
                                    style: const TextStyle(
                                        fontSize: 25,

                                        color: Colors.white
                                    ),)
                                ],
                              ),
                            ),
                          ),
                        ),


                      ],
                    ),
                    const SizedBox(height: 10,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Card(
                        elevation: 7,
                        child: ListTile(
                          tileColor: Colors.blue,
                          title: const Text('Today Cases', style: TextStyle(
                              fontSize: 20,

                              fontWeight: FontWeight.bold,

                              color: Colors.white
                          ),),
                          trailing: Text(
                            _covidInfoProvider.covidModel!.todayCases.toString(),
                            style: const TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold,
                                color: Colors.white
                            ),),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Card(
                        elevation: 7,
                        child: ListTile(
                          tileColor: Colors.green,
                          title: const Text('Today Recoverd', style: TextStyle(
                              fontSize: 20,

                              fontWeight: FontWeight.bold,

                              color: Colors.white
                          ),),
                          trailing: Text(
                            _covidInfoProvider.covidModel!.todayRecovered
                                .toString(), style: const TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold,
                              color: Colors.white
                          ),),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Card(
                        elevation: 7,
                        child: ListTile(
                          tileColor: Colors.red,
                          title: const Text('Today Deaths', style: TextStyle(
                              fontSize: 20,

                              fontWeight: FontWeight.bold,

                              color: Colors.white
                          ),),
                          trailing: Text(
                            _covidInfoProvider.covidModel!.todayDeaths.toString(),
                            style: const TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold,
                                color: Colors.white
                            ),),
                        ),
                      ),
                    ),

                  ],
                )
              ],
            ),
          ),
        ) : const Center(child: CircularProgressIndicator(
          color: Colors.white,
        ))
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(4, (i) {
      final isTouched = i == touchedIndex;
      final fontSize = isTouched ? 25.0 : 16.0;
      final radius = isTouched ? 60.0 : 50.0;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.blue,
            value:(_covidInfoProvider.covidModel!.casesPerOneMillion!.toDouble())/10000,
            title: '${(_covidInfoProvider.covidModel!.casesPerOneMillion!/10000).toStringAsFixed(2)}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: Colors.red,
            value:(_covidInfoProvider.covidModel!.deathsPerOneMillion!.toDouble())/10000,
            title: '${(_covidInfoProvider.covidModel!.deathsPerOneMillion!/10000).toStringAsFixed(2)}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color:Colors.deepPurple,
            value:(_covidInfoProvider.covidModel!.testsPerOneMillion!.toDouble())/10000,
            title: '${(_covidInfoProvider.covidModel!.testsPerOneMillion!/10000).toStringAsFixed(2)}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 3:
          return PieChartSectionData(
            color: Colors.green,
            value:(_covidInfoProvider.covidModel!.recoveredPerOneMillion!.toDouble())/10000,
            title: '${(_covidInfoProvider.covidModel!.recoveredPerOneMillion!/10000).toStringAsFixed(2)}%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        default:
          throw Error();
      }
    });
  }
}

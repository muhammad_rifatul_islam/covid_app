
import 'package:covid_19/pages/covidInfoPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName='/splash';
  const SplashScreen({Key? key}) : super(key: key);



  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {



    Future.delayed(const Duration(seconds: 5),
            (){
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context)=>CovidInfoPage()));
        }
    );

    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: const Color(0xff090935),

          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Covid-19",style:GoogleFonts.lobster(
                    textStyle: const TextStyle(
                        fontSize: 70,
                        color: Colors.white
                    )
                )
                ),
                SizedBox(height: 40,),
                Lottie.asset('assets/animation/covid.json',
                    height: 250)
              ],
            ),
          ),
        )

    );
  }
}

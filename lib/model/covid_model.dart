/// updated : 1664427421112
/// country : "Bangladesh"
/// countryInfo : {"_id":50,"iso2":"BD","iso3":"BGD","lat":24,"long":90,"flag":"https://disease.sh/assets/img/flags/bd.png"}
/// cases : 2023810
/// todayCases : 0
/// deaths : 29360
/// todayDeaths : 0
/// recovered : 1964147
/// todayRecovered : 0
/// active : 30303
/// critical : 1331
/// casesPerOneMillion : 12021
/// deathsPerOneMillion : 174
/// tests : 14867243
/// testsPerOneMillion : 88307
/// population : 168358854
/// continent : "Asia"
/// oneCasePerPeople : 83
/// oneDeathPerPeople : 5734
/// oneTestPerPeople : 11
/// activePerOneMillion : 179.99
/// recoveredPerOneMillion : 11666.43
/// criticalPerOneMillion : 7.91

class CovidModel {
  CovidModel({
      this.updated, 
      this.country, 
      this.countryInfo, 
      this.cases, 
      this.todayCases, 
      this.deaths, 
      this.todayDeaths, 
      this.recovered, 
      this.todayRecovered, 
      this.active, 
      this.critical, 
      this.casesPerOneMillion, 
      this.deathsPerOneMillion, 
      this.tests, 
      this.testsPerOneMillion, 
      this.population, 
      this.continent, 
      this.oneCasePerPeople, 
      this.oneDeathPerPeople, 
      this.oneTestPerPeople, 
      this.activePerOneMillion, 
      this.recoveredPerOneMillion, 
      this.criticalPerOneMillion,});

  CovidModel.fromJson(dynamic json) {
    updated = json['updated'];
    country = json['country'];
    countryInfo = json['countryInfo'] != null ? CountryInfo.fromJson(json['countryInfo']) : null;
    cases = json['cases'];
    todayCases = json['todayCases'];
    deaths = json['deaths'];
    todayDeaths = json['todayDeaths'];
    recovered = json['recovered'];
    todayRecovered = json['todayRecovered'];
    active = json['active'];
    critical = json['critical'];
    casesPerOneMillion = json['casesPerOneMillion'];
    deathsPerOneMillion = json['deathsPerOneMillion'];
    tests = json['tests'];
    testsPerOneMillion = json['testsPerOneMillion'];
    population = json['population'];
    continent = json['continent'];
    oneCasePerPeople = json['oneCasePerPeople'];
    oneDeathPerPeople = json['oneDeathPerPeople'];
    oneTestPerPeople = json['oneTestPerPeople'];
    activePerOneMillion = json['activePerOneMillion'];
    recoveredPerOneMillion = json['recoveredPerOneMillion'];
    criticalPerOneMillion = json['criticalPerOneMillion'];
  }
  num? updated;
  String? country;
  CountryInfo? countryInfo;
  num? cases;
  num? todayCases;
  num? deaths;
  num? todayDeaths;
  num? recovered;
  num? todayRecovered;
  num? active;
  num? critical;
  num? casesPerOneMillion;
  num? deathsPerOneMillion;
  num? tests;
  num? testsPerOneMillion;
  num? population;
  String? continent;
  num? oneCasePerPeople;
  num? oneDeathPerPeople;
  num? oneTestPerPeople;
  num? activePerOneMillion;
  num? recoveredPerOneMillion;
  num? criticalPerOneMillion;
CovidModel copyWith({  num? updated,
  String? country,
  CountryInfo? countryInfo,
  num? cases,
  num? todayCases,
  num? deaths,
  num? todayDeaths,
  num? recovered,
  num? todayRecovered,
  num? active,
  num? critical,
  num? casesPerOneMillion,
  num? deathsPerOneMillion,
  num? tests,
  num? testsPerOneMillion,
  num? population,
  String? continent,
  num? oneCasePerPeople,
  num? oneDeathPerPeople,
  num? oneTestPerPeople,
  num? activePerOneMillion,
  num? recoveredPerOneMillion,
  num? criticalPerOneMillion,
}) => CovidModel(  updated: updated ?? this.updated,
  country: country ?? this.country,
  countryInfo: countryInfo ?? this.countryInfo,
  cases: cases ?? this.cases,
  todayCases: todayCases ?? this.todayCases,
  deaths: deaths ?? this.deaths,
  todayDeaths: todayDeaths ?? this.todayDeaths,
  recovered: recovered ?? this.recovered,
  todayRecovered: todayRecovered ?? this.todayRecovered,
  active: active ?? this.active,
  critical: critical ?? this.critical,
  casesPerOneMillion: casesPerOneMillion ?? this.casesPerOneMillion,
  deathsPerOneMillion: deathsPerOneMillion ?? this.deathsPerOneMillion,
  tests: tests ?? this.tests,
  testsPerOneMillion: testsPerOneMillion ?? this.testsPerOneMillion,
  population: population ?? this.population,
  continent: continent ?? this.continent,
  oneCasePerPeople: oneCasePerPeople ?? this.oneCasePerPeople,
  oneDeathPerPeople: oneDeathPerPeople ?? this.oneDeathPerPeople,
  oneTestPerPeople: oneTestPerPeople ?? this.oneTestPerPeople,
  activePerOneMillion: activePerOneMillion ?? this.activePerOneMillion,
  recoveredPerOneMillion: recoveredPerOneMillion ?? this.recoveredPerOneMillion,
  criticalPerOneMillion: criticalPerOneMillion ?? this.criticalPerOneMillion,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['updated'] = updated;
    map['country'] = country;
    if (countryInfo != null) {
      map['countryInfo'] = countryInfo?.toJson();
    }
    map['cases'] = cases;
    map['todayCases'] = todayCases;
    map['deaths'] = deaths;
    map['todayDeaths'] = todayDeaths;
    map['recovered'] = recovered;
    map['todayRecovered'] = todayRecovered;
    map['active'] = active;
    map['critical'] = critical;
    map['casesPerOneMillion'] = casesPerOneMillion;
    map['deathsPerOneMillion'] = deathsPerOneMillion;
    map['tests'] = tests;
    map['testsPerOneMillion'] = testsPerOneMillion;
    map['population'] = population;
    map['continent'] = continent;
    map['oneCasePerPeople'] = oneCasePerPeople;
    map['oneDeathPerPeople'] = oneDeathPerPeople;
    map['oneTestPerPeople'] = oneTestPerPeople;
    map['activePerOneMillion'] = activePerOneMillion;
    map['recoveredPerOneMillion'] = recoveredPerOneMillion;
    map['criticalPerOneMillion'] = criticalPerOneMillion;
    return map;
  }

}

/// _id : 50
/// iso2 : "BD"
/// iso3 : "BGD"
/// lat : 24
/// long : 90
/// flag : "https://disease.sh/assets/img/flags/bd.png"

class CountryInfo {
  CountryInfo({
      this.id, 
      this.iso2, 
      this.iso3, 
      this.lat, 
      this.long, 
      this.flag,});

  CountryInfo.fromJson(dynamic json) {
    id = json['_id'];
    iso2 = json['iso2'];
    iso3 = json['iso3'];
    lat = json['lat'];
    long = json['long'];
    flag = json['flag'];
  }
  num? id;
  String? iso2;
  String? iso3;
  num? lat;
  num? long;
  String? flag;
CountryInfo copyWith({  num? id,
  String? iso2,
  String? iso3,
  num? lat,
  num? long,
  String? flag,
}) => CountryInfo(  id: id ?? this.id,
  iso2: iso2 ?? this.iso2,
  iso3: iso3 ?? this.iso3,
  lat: lat ?? this.lat,
  long: long ?? this.long,
  flag: flag ?? this.flag,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['iso2'] = iso2;
    map['iso3'] = iso3;
    map['lat'] = lat;
    map['long'] = long;
    map['flag'] = flag;
    return map;
  }

}